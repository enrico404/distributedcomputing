package com.company;
import java.rmi.*;
import java.rmi.server.UnicastRemoteObject;

public class HelloImpl extends UnicastRemoteObject implements Hello {
    private String name;
    public HelloImpl(String s) throws RemoteException{
        super();
        name = s;
    }


    public static void main(String args[]) throws RemoteException {
       if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
       }

        try {
            HelloImpl obj = new HelloImpl("HelloServer");
            Naming.rebind("//localhost/HelloServer", obj);
            System.out.println();
            System.out.println("HelloImpl: HelloServer bound in registry");
            System.out.println();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public String sayHello() throws RemoteException {
        return "Hello World!";
    }
}
