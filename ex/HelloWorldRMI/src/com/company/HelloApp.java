package com.company;

import com.company.Hello;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class HelloApp {
    public static void main(String args[]){
        String message="";
        String server = "//localhost/HelloServer";

        try {
            Hello obj = (Hello) Naming.lookup(server);
            message = obj.sayHello();
            System.out.println("message received: "+ message);
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }
}
